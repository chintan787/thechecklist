import AsyncStorage from "@react-native-community/async-storage";
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from "redux-persist";
import thunk from "redux-thunk";
import logger from "redux-logger";
import reducers from '@redux/reducers'

let middleware = [thunk];
if (process.env.NODE_ENV === `development`) {
    middleware.push(logger);
}

const store = compose(applyMiddleware(...middleware))(createStore)(reducers)
const persistor = persistStore(store);

export { store, persistor };
