import React, { Component } from "react";
import { connect } from "react-redux";
import { AuthActions } from "@actions";
import { ActivityIndicator, View,ImageBackground } from "react-native";
import { bindActionCreators } from "redux";
import { Images, BaseColor, BaseSetting } from "@config";
import SplashScreen from "react-native-splash-screen";
import { Image, Text } from "@components";
import styles from "./styles";

class Loading extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        SplashScreen.hide();
        setTimeout(() => {
            this.props.navigation.navigate("Main");
        }, 1000);
    }

    render() {
        return (
        <ImageBackground source={Images.splash} style={styles.container} resizeMode={"cover"} >
                <View style={{ alignItems: "center" }}>
                    <Image
                        source={Images.splashLogo}
                        style={styles.logo}
                        resizeMode="contain"
                    />
                  
                </View>
                <ActivityIndicator
                    size="large"
                    color={'#fff'}
                    style={{
                        position: "absolute",
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                />
            </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(AuthActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Loading);
