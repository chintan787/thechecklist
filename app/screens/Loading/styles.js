import { StyleSheet } from "react-native";
import { BaseColor } from "@config";

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center"
    },
    logo: {
        top:"80%",
        width: 250,
        height: 120
    }
});
