import React, { Component } from "react";
import {
    View,
    ScrollView,
    FlatList,
    Animated,
    TouchableOpacity,
    Linking,
    Alert
} from "react-native";
import { fetchPostRecent, fetchCategories } from '@redux/actions'
import { connect } from 'react-redux'
import { BaseStyle, BaseColor } from "@config";
import { Config, Images, Tools } from '@common'
import {
    Header,
    SafeAreaView,
    Icon,
    Text,
    StarRating,
    Tag,
    Image,
    PlaceItem,
    CardList
} from "@components";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import * as Utils from "@utils";
import styles from "./styles";

// Load sample data
import { PlaceListData, ReviewData } from "@data";

class PlaceDetail extends Component {
    constructor(props) {
        super(props);

        // Temp data define
        this.state = {
            post: false,
            collapseHour: true,
            index: 0,
            routes: [
                { key: "information", title: "Information" },
                { key: "review", title: "Review" },
                { key: "feedback", title: "Feedback" },
                { key: "map", title: "Map" }
            ],
            heightHeader: Utils.heightHeader(),
            information:[],
            // workHours: [
            //     { id: "1", date: "Monday", hour: "09:0 AM - 18:00 PM" },
            //     { id: "2", date: "Tuesday", hour: "09:0 AM - 18:00 PM" },
            //     { id: "3", date: "Wednesday", hour: "09:0 AM - 18:00 PM" },
            //     { id: "4", date: "Thursday", hour: "09:0 AM - 18:00 PM" },
            //     { id: "5", date: "Friday", hour: "09:0 AM - 18:00 PM" },
            //     { id: "6", date: "Saturday", hour: "Close" },
            //     { id: "7", date: "Sunday", hour: "Close" }
            // ],
            list: PlaceListData,
            relate: PlaceListData.slice(2, 4),
            // facilities: [
            //     { id: "1", icon: "wifi", name: "Free Wifi", checked: true },
            //     { id: "2", icon: "bath", name: "Shower" },
            //     { id: "3", icon: "paw", name: "Pet Allowed" },
            //     { id: "4", icon: "bus", name: "Shuttle Bus" },
            //     { id: "5", icon: "cart-plus", name: "Supper Market" },
            //     { id: "6", icon: "clock", name: "Open 24/7" }
            // ],
            
        };
        this._deltaY = new Animated.Value(0);
    }

    componentDidMount() {
        const post = this.props.navigation.getParam('postItem', false);
        console.log('postItem ===== ', post);
    

        if (post) {
            this.setState({post: post})
            this.setState({
                information: [
                    {
                        id: "1",
                        icon: "map-marker-alt",
                        title: "Address",
                        type: "map",
                        information: post.listing_data._job_location
                    }
                    // {
                    //     id: "2",
                    //     icon: "mobile-alt",
                    //     title: "Tel",
                    //     type: "phone",
                    //     information: "+903 9802-7892"
                    // },
                    // {
                    //     id: "3",
                    //     icon: "envelope",
                    //     title: "Email",
                    //     type: "email",
                    //     information: "liststar@passionui.com"
                    // },
                    // {
                    //     id: "4",
                    //     icon: "globe",
                    //     title: "Website",
                    //     type: "web",
                    //     information: "http://passionui.com"
                    // }
                ]
            })
        }
    }

    filterPostItem (post) {
        console.log(post);
        const { small } = Config.PostImage
        const imageURL =
            post
                ? Tools.getImage(post, 'large', true)
                : Images.imageBase
        const title =
            post
                ? Tools.formatText(post.title.rendered, 300)
                : ' '
        const description =
            post ? Tools.getDescription(post.content.rendered, 200000) : ' ';
        const subDesc =
            post ? Tools.getDescription(post.content.rendered, 200) : ' ';
        const tagline =  post ? post.listing_data._company_tagline : ' ';
        const rating = post ? post.totalRate : 0;
        const totalReview = post ? post.totalReview : 0;
        const price = post ? post.cost : '';
        const latitude = post ? post.geolocation_lat * 1 : 37.763844;
        const longitude = post ? post.geolocation_long * 1 : -122.414925;
        const location = post && typeof post.listing_data !== 'undefined' &&
            typeof post.listing_data.geolocation_formatted_address !== 'undefined' ? post.listing_data.geolocation_formatted_address : '';
        const status = post ? (post.listing_data._featured == "1" ? 'Featured':null): '';  
        let categories = [];
        if ( post && typeof post.pure_taxonomies !== 'undefined' && typeof post.pure_taxonomies.job_listing_category !== 'undefined' && post.pure_taxonomies.job_listing_category.length > 0) {
            const job_listing_category = post.pure_taxonomies.job_listing_category;
            job_listing_category.forEach(catItem => {
                categories.push(catItem.name)
            })
        }

        return {
            imageURL,
            title,
            description,
            subDesc,
            tagline,
            location,
            latitude,
            longitude,
            rating,
            totalReview,
            price,
            categories,

            region: {
                latitude: latitude,
                longitude: longitude,
                latitudeDelta: 0.009,
                longitudeDelta: 0.004
            },
            status,
            rateStatus: "Very Good"
           
        
        }
    }

    onOpen(item,title) {
        Alert.alert(
            title,
            "Do you want to open " + item.title + " ?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "OK",
                    onPress: () => {
                        switch (item.type) {
                            case "web":
                                Linking.openURL(item.information);
                                break;
                            case "phone":
                                Linking.openURL("tel://" + item.information);
                                break;
                            case "email":
                                Linking.openURL("mailto:" + item.information);
                                break;
                            case "map":
                                Linking.openURL(
                                    "http://maps.apple.com/?ll=37.484847,-122.148386"
                                );
                                break;
                        }
                    }
                }
            ],
            { cancelable: true }
        );
    }

    onCollapse() {
        Utils.enableExperimental();
        this.setState({
            collapseHour: !this.state.collapseHour
        });
    }

    render() {
        const { navigation, list } = this.props;
        list.length = 3;
        const {
            post,
            heightHeader,
            information,
            workHours,
            collapseHour,
            relate,
            facilities,

        } = this.state;
        console.log('post ====', post);
        const heightImageBanner = Utils.scaleWithPixel(250, 1);
        const  {imageURL, title, description,tagline, latitude, longitude,region,categories, location,  rating, totalReview, status, rateStatus} = this.filterPostItem(post);
        return (
            <View style={{ flex: 1 }}>
                <Animated.View
                    style={[
                        styles.imgBanner,
                        {
                            height: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [
                                    heightImageBanner,
                                    heightHeader,
                                    heightHeader
                                ]
                            })
                        }
                    ]}
                >
                    <Image source={{uri: imageURL}} style={{ flex: 1 }} />
                    <Animated.View
                        style={{
                            position: "absolute",
                            bottom: 15,
                            left: 20,
                            flexDirection: "row",
                            opacity: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [1, 0, 0]
                            })
                        }}
                    >
                        {/* <Image
                            source={Images.profile2}
                            style={styles.userIcon}
                        />
                        <View>
                            <Text headline semibold whiteColor>
                                Steve Garrett
                            </Text>
                            <Text footnote whiteColor>
                                5 hours ago | 100k views
                            </Text>
                        </View> */}
                    </Animated.View>
                </Animated.View>
                <SafeAreaView
                    style={BaseStyle.safeAreaView}
                    forceInset={{ top: "always" }}
                >
                    {/* Header */}
                    <Header
                        title=""
                        renderLeft={() => {
                            return (
                                <Icon
                                    name="arrow-left"
                                    size={20}
                                    color={BaseColor.whiteColor}
                                />
                            );
                        }}
                        renderRight={() => {
                            return (
                                <Icon
                                    name="images"
                                    size={20}
                                    color={BaseColor.whiteColor}
                                />
                            );
                        }}
                        onPressLeft={() => {
                            navigation.goBack();
                        }}
                        onPressRight={() => {
                            navigation.navigate("PreviewImage",{postItem: post});
                        }}
                    />
                    <ScrollView
                        onScroll={Animated.event([
                            {
                                nativeEvent: {
                                    contentOffset: { y: this._deltaY }
                                }
                            }
                        ])}
                        onContentSizeChange={() => {
                            this.setState({
                                heightHeader: Utils.heightHeader()
                            });
                        }}
                        scrollEventThrottle={8}
                    >
                        <View style={{ height: 255 - heightHeader }} />
                        <View
                            style={{
                                paddingHorizontal: 20,
                                marginBottom: 20
                            }}
                        >
                            <View style={styles.lineSpace}>
                                <Text title1 semibold>
                                    {title}
                                </Text>
                                <Icon
                                    name="heart"
                                    color={BaseColor.lightPrimaryColor}
                                    size={24}
                                />
                            </View>
                            <View style={styles.lineSpace}>
                                <View>
                                    <Text caption1 grayColor>
                                        {tagline}
                                        
                                    </Text>
                                    {/* <TouchableOpacity
                                        style={styles.rateLine}
                                        onPress={() =>
                                            navigation.navigate("Review")
                                        }
                                    >
                                        <Tag
                                            rateSmall
                                            style={{ marginRight: 5 }}
                                            onPress={() =>
                                                navigation.navigate("Review")
                                            }
                                        >
                                            4.5
                                        </Tag>
                                        <StarRating
                                            disabled={true}
                                            starSize={10}
                                            maxStars={5}
                                            rating={4.5}
                                            fullStarColor={
                                                BaseColor.yellowColor
                                            }
                                            on
                                        />
                                        <Text
                                            footnote
                                            grayColor
                                            style={{ marginLeft: 5 }}
                                        >
                                            (609)
                                        </Text>
                                    </TouchableOpacity> */}
                                </View>
                                {
                                    status!=null?
                                    <Tag status>
                                    {status}
                                    </Tag>
                                    :null
                                }
                                
                            </View>
                            {information.map(item => {
                                return (
                                    <TouchableOpacity
                                        style={styles.line}
                                        key={item.id}
                                        onPress={() => this.onOpen(item,title)}
                                    >
                                        <View style={styles.contentIcon}>
                                            <Icon
                                                name={item.icon}
                                                size={16}
                                                color={BaseColor.whiteColor}
                                            />
                                        </View>
                                        <View style={{ marginLeft: 10 }}>
                                            <Text caption2 grayColor>
                                                {item.title}
                                            </Text>
                                            <Text
                                                footnote
                                                semibold
                                                style={{ marginTop: 5 }}
                                            >
                                                {item.information}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                );
                            })}
                            {/* <TouchableOpacity
                                style={styles.line}
                                onPress={() => this.onCollapse()}
                            >
                                <View style={styles.contentIcon}>
                                    <Icon
                                        name="clock"
                                        size={16}
                                        color={BaseColor.whiteColor}
                                    />
                                </View>
                                <View style={styles.contentInforAction}>
                                    <View>
                                        <Text caption2 grayColor>
                                            Open Hours
                                        </Text>
                                        <Text
                                            footnote
                                            semibold
                                            style={{ marginTop: 5 }}
                                        >
                                            09:00 AM - 18:00 PM
                                        </Text>
                                    </View>
                                    <Icon
                                        name={
                                            collapseHour
                                                ? "angle-up"
                                                : "angle-down"
                                        }
                                        size={24}
                                        color={BaseColor.grayColor}
                                    />
                                </View>
                            </TouchableOpacity>
                            <View
                                style={{
                                    paddingLeft: 40,
                                    paddingRight: 20,
                                    marginTop: 5,
                                    height: collapseHour ? 0 : null,
                                    overflow: "hidden"
                                }}
                            >
                                {workHours.map(item => {
                                    return (
                                        <View
                                            style={styles.lineWorkHours}
                                            key={item.id}
                                        >
                                            <Text body2 grayColor>
                                                {item.date}
                                            </Text>
                                            <Text body2 accentColor semibold>
                                                {item.hour}
                                            </Text>
                                        </View>
                                    );
                                })}
                            </View> */}
                        </View>
                        <View style={styles.contentDescription}>
                            <Text body2 style={{ lineHeight: 20,marginBottom:20 }}>
                                {description}
                            </Text>
                            {/* <View
                                style={{
                                    paddingVertical: 20,
                                    flexDirection: "row",
                                    borderBottomWidth: 1,
                                    borderColor: BaseColor.textSecondaryColor
                                }}
                            >
                                <View style={{ flex: 1 }}>
                                    <Text caption1 grayColor>
                                        Date Established
                                    </Text>
                                    <Text headline style={{ marginTop: 5 }}>
                                        Sep 26, 2009
                                    </Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text caption1 grayColor>
                                        Price Range
                                    </Text>
                                    <Text headline style={{ marginTop: 5 }}>
                                        $46.00 to $93.00
                                    </Text>
                                </View>
                            </View> */}
                            <View
                                style={{
                                    height: 180,
                                    paddingVertical: 20
                                }}
                            >
                                <MapView
                                    provider={PROVIDER_GOOGLE}
                                    style={styles.map}
                                    region={region}  
                                    onRegionChange={() => {}}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: latitude,
                                            longitude: longitude
                                        }}
                                        
                                        
                                    />
                                </MapView>
                            </View>
                        </View>
                        {/*
                        <Text
                            title3
                            semibold
                            style={{
                                paddingHorizontal: 20,
                                paddingTop: 15,
                                paddingBottom: 5
                            }}
                        >
                            Facilities
                        </Text>
                        <View style={styles.wrapContent}>
                            {facilities.map(item => {
                                return (
                                    <Tag
                                        icon={
                                            <Icon
                                                name={item.icon}
                                                size={12}
                                                color={BaseColor.accentColor}
                                                solid
                                                style={{ marginRight: 5 }}
                                            />
                                        }
                                        chip
                                        key={item.id}
                                        style={{
                                            marginTop: 10,
                                            marginRight: 10
                                        }}
                                    >
                                        {item.name}
                                    </Tag>
                                );
                            })}
                        </View>
                         <Text
                            title3
                            semibold
                            style={{
                                paddingHorizontal: 20,
                                paddingVertical: 15
                            }}
                        >
                            Featured
                        </Text>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={list}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({ item, index }) => {
                                const  {imageURL, title, subDesc, location, rating, totalReview, status, rateStatus} = this.filterPostItem(item);
                                return (
                                    <PlaceItem
                                        gridBox
                                        image={{uri: imageURL}}
                                        title={title}
                                        subtitle={subDesc}
                                        location={location}
                                        rate={rating}
                                        status={status}
                                        rateStatus={rateStatus}
                                        numReviews={totalReview}
                                        onPress={() =>
                                            navigation.navigate("PlaceDetail", {postItem: item})
                                        }
                                        onPressTag={() =>
                                            navigation.navigate("Review")
                                        }
                                        style={{ marginLeft: 20 }}
                                    />
                                )
                            }}
                        />
                        <Text
                            title3
                            semibold
                            style={{
                                paddingHorizontal: 20,
                                paddingVertical: 15
                            }}
                        >
                            Related
                        </Text>
                        <FlatList
                            contentContainerStyle={{
                                marginHorizontal: 20
                            }}
                            data={list}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({ item, index }) => {
                                const  {imageURL, title, subDesc, location, rating, totalReview, status, rateStatus} = this.filterPostItem(item);
                                return (
                                    <CardList
                                        image={{uri: imageURL}}
                                        title={title}
                                        subtitle={subDesc}
                                        rate={rating}
                                        style={{ marginBottom: 20 }}
                                        onPress={() =>
                                            navigation.navigate("PlaceDetail", {postItem: item})
                                        }
                                        onPressTag={() =>
                                            navigation.navigate("Review")
                                        }
                                    />
                                )
                            }}
                        /> */}
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const mapStateToProps = ({ posts, config }, ownProps) => {
    return {
        list: posts.list,
        layout: ownProps.layout ? ownProps.layout : config.verticalLayout,
    }
}

export default connect(
    mapStateToProps,
    { fetchPostRecent }
)(PlaceDetail)
