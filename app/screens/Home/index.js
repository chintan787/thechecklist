import React, { Component } from "react";
import {
  View,
  ScrollView,
  Animated,
  TouchableOpacity,
  FlatList,
  TextInput
} from "react-native";
import {
  Image,
  Text,
  Icon,
  Card,
  SafeAreaView,
  Tag,
  CardList
} from "@components";
import { fetchPostRecent, fetchCategories,fetchGroups } from '@redux/actions'
import { connect } from 'react-redux'
import { Config, AppConfig, Images, Tools } from '@common';
import { BaseStyle, BaseColor } from "@config";
import * as Utils from "@utils";
import styles from "./styles";
import Swiper from "react-native-swiper";
import {
  HomeServicesData,
  HomePopularData,
  HomeListData,
  HomeBannerData
} from "@data";

class Home extends Component {
  constructor(props) {
    super(props);

    // Temp data define
    this.state = {
      banner: HomeBannerData,
      location: [
        { id: "1", name: "Delux Room" },
        { id: "2", name: "Tripple Room" },
        { id: "3", name: "Single Room" },
        { id: "4", name: "King Room" },
        { id: "5", name: "King Room" }
      ],
      services: HomeServicesData,
      popular: HomePopularData,
      list: HomeListData,
      search: '',
      heightHeader: Utils.heightHeader(),
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.009,
        longitudeDelta: 0.004
      },
      categoriesList:[]
    };
    this._deltaY = new Animated.Value(0);
  }

  componentDidMount() {
    this.props.fetchPostRecent(1)
    this.props.fetchCategories()
    this.props.fetchGroups() 
  }

  onSearch(search){
    const { navigation } = this.props;
    if(search != ""){
    navigation.navigate("Place",{searchText: search,searchFetch:true,isMap:true});
    this.setState({
      search:""
    })
}

}
    
  /**
   * @description Show location on form searching
   * @author Passion UI <passionui.com>
   * @date 2019-09-01
   * @returns
   */
  renderLocation() {
    const { navigation } = this.props;
    return this.state.location.map((item, i) => {
      return (
        <Tag
          gray
          key={item.id}
          onPress={() => {}}
          style={{
            backgroundColor: BaseColor.fieldColor,
            marginTop: 5
          }}
        >
          <Text caption2>{item.name}</Text>
        </Tag>
      );
    });
  }

  renderGroups(item, index) {
   
   
    const { navigation } = this.props;
    // optimize image
    let imageURL = "";
    if (item.image && item.image[0]) {
      let temp = item.image[0].file
      imageURL = temp ? `${AppConfig.Website.url}/wp-content/uploads/${temp}` : "";
    } else {
      imageURL = Images.imageBase;
    }

    const title =
        typeof item !== 'undefined'
            ? Tools.formatText(item.name, 300)
            : ' ';

    return (
        <Card
           image={{ uri: imageURL }}
            style={[
              styles.popularItem,
              index == 0 ? { marginHorizontal: 20 } : { marginRight: 20 }
            ]}
            
            onPress={() =>   navigation.navigate("Place", {itemId: item.id,groupFetch:true})}
        >
          <Text headline whiteColor semibold>
            {title}
          </Text>
        </Card>
    )
  }

  renderRecentItem(item, index) {
    
    const { navigation } = this.props;
    const { small } = Config.PostImage;
    const imageURL =
        typeof item !== 'undefined'
            ? Tools.getImage(item, "large", true)
            : Images.imageBase;
    const title =
        typeof item !== 'undefined'
            ? Tools.formatText(item.title.rendered, 300)
            : ' ';
    const description =
        typeof item !== 'undefined' ? Tools.getDescription(item.content, 200) : ' ';
    const rating = typeof item !== 'undefined' ? item.totalRate : 0;
    const tagline =  typeof item !== 'undefined' ? item.listing_data._company_tagline : ' ';
    const status = typeof item !== 'undefined' ? (item.listing_data._featured == "1" ? 'Featured':null): ' ';

  if(status == 'Featured'){

  
    return (
        <CardList
          image={{ uri: imageURL }}
          title={title}
          subtitle={tagline}
          rate={rating}
          style={{ marginBottom: 20 }}
          onPress={() =>   navigation.navigate("PlaceDetail", {postItem: item})}
        />
    )
  }
 
  }

  render() {
    const { navigation, list, categories, groups } = this.props;
    const { banner, services, popular, heightHeader } = this.state;
    const heightImageBanner = Utils.scaleWithPixel(225);
    const marginTopBanner = heightImageBanner - heightHeader - 20;
    return (
      <View style={{ flex: 1 }}>
        <Animated.View
          style={[
            styles.imageBackground,
            {
              height: this._deltaY.interpolate({
                inputRange: [
                  0,
                  Utils.scaleWithPixel(150),
                  Utils.scaleWithPixel(150)
                ],
                outputRange: [heightImageBanner, heightHeader, 0]
              })
            }
          ]}
        >
          <Swiper
            dotStyle={{
              backgroundColor: BaseColor.textSecondaryColor
            }}
            activeDotColor={BaseColor.primaryColor}
            paginationStyle={styles.contentPage}
            removeClippedSubviews={false}
            autoplay={true}
            autoplayTimeout={2}
          >
            {banner.map((item, index) => {
              return (
                <Image key={item.id} source={item.image} style={{ flex: 1 }} />
              );
            })}
          </Swiper>
        </Animated.View>
        <SafeAreaView
          style={BaseStyle.safeAreaView}
          forceInset={{ top: "always" }}
        >
          <ScrollView
            onScroll={Animated.event([
              {
                nativeEvent: {
                  contentOffset: { y: this._deltaY }
                }
              }
            ])}
            onContentSizeChange={() => {
              this.setState({
                heightHeader: Utils.heightHeader()
              });
            }}
            scrollEventThrottle={8}
          >
            <View style={[styles.searchForm, { marginTop: marginTopBanner }]}>
              <View
                  style={[
                    BaseStyle.textInput,
                    {
                      flexDirection: "row",
                      alignItems: "center"
                    }
                  ]}
              >
                <TextInput
                    style={[{ flex: 1 }, BaseStyle.textInput]}
                    onSubmitEditing={() => {
                      this.onSearch(this.state.search);
                  }}
                    onChangeText={text =>
                        this.setState({ search: text })
                    }
                    placeholder="Search"
                    placeholderTextColor={BaseColor.grayColor}
                    value={this.state.search}
                    selectionColor={BaseColor.primaryColor}
                />
                <View style={styles.lineForm} />
                <Icon
                    name="location-arrow"
                    size={18}
                    color={BaseColor.lightPrimaryColor}
                    solid
                    onPress={() => {
                      this.onSearch(this.state.search);
                  }}
                />
              </View>
              {/* <View style={styles.contentLocation}>
                {this.renderLocation()}
              </View> */}
            </View>
            {/* services */}
            <FlatList
              contentContainerStyle={{ padding: 20 }}
              data={categories.slice(0,8)}
              numColumns={4}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item,index}) => {
                return (
                    <TouchableOpacity
                      style={styles.serviceItem}
                      onPress={() => navigation.navigate(index!=7? "Place":"Category", {itemId: item.id,categoryFetch:true})}
                      
                    >
                      <View
                        style={[
                          styles.serviceCircleIcon,
                          { backgroundColor: '#383853' }
                        ]}
                      >    
                        <Icon
                          name={
                            index!=7 ?
                            "coffee"
                            :
                            "ellipsis-h"
                          }        
                          size={20}
                          color={BaseColor.whiteColor}
                          solid
                        />
                      </View>
                      <Text
                        footnote
                        style={{
                          marginTop: 5,
                          marginBottom: 20
                        }}
                      >
                      {
                        index!=7 ?
                        item.name
                        :
                        "More"
                      }        
                      </Text>    
                    </TouchableOpacity>
                   );
              }}
            />
            {/* Hiking */}
            <View style={styles.contentPopular}>
              <Text title3 semibold>
                Occasion
              </Text>
              <Text body2 grayColor>
                Find the right activity for the right occasion
              </Text>
            </View>
            <FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={groups}
              keyExtractor={(item, index) => item.id}
              renderItem={({ item, index }) => this.renderGroups(item, index)}
            />
            <View
              style={{
                padding: 20
              }}
            >
              <Text title3 semibold>
                Featured
              </Text>
              <Text body2 grayColor>
                The best of the bunch
              </Text>
              <FlatList
                style={{ marginTop: 20 }}
                data={list}
                keyExtractor={(item, index) => item.id}
                renderItem={({ item, index }) => this.renderRecentItem(item, index)}
              />
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}


const mapStateToProps = ({ categories, posts, groups, config}, ownProps) => {
  return {
    categories: categories.list,
    list: posts.list,
    groups: groups.list,
    layout: ownProps.layout ? ownProps.layout : config.verticalLayout,
  }
}

export default connect(
    mapStateToProps,
    { fetchPostRecent, fetchCategories, fetchGroups }
)(Home)
