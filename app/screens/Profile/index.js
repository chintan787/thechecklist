import React, { Component } from "react";
import { View, ScrollView, TouchableOpacity, Switch } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AuthActions } from "@actions";
import { BaseStyle, BaseColor, BaseSetting } from "@config";
import { Events, Config, Languages } from "@common";
import { store } from "app/store";
import { User } from "@services";
import { clearUserData } from "@redux/actions";
import {
  Header,
  SafeAreaView,
  Icon,
  Text,
  Button,
  ProfileDetail,
  ProfilePerformance
} from "@components";
import styles from "./styles";

class Profile extends Component {
  constructor(props) {
    super(props);
    let { userData } = "";
    if (store.getState().user.data != null) {
      userData = store.getState().user.data;
    }
    this.state = {
      notification: false,
      loading: false,
      userData: userData
    };
  }

  updateLoading = status => this.setState({ loading: status });

  btnLogOut = async () => {
    const { navigation, netInfo } = this.props;
    if (!netInfo.isConnected) {
      return Events.toast(Languages.noConnection);
    }

    this.updateLoading(true);
    User.logOut();
    this.props.clearUserData();
    setTimeout(() => {
      this.updateLoading(false);
      navigation.navigate("Home");
    }, 500);
  };
  /**
   * @description Simple logout with Redux
   * @author Passion UI <passionui.com>
   * @date 2019-09-01
   */
  /*onLogOut() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.actions.authentication(false, response => {
          if (response.success) {
            this.props.navigation.navigate("Loading");
          } else {
            this.setState({ loading: false });
          }
        });
      }
    );
  }*/

  /**
   * @description Call when reminder option switch on/off
   */
  toggleSwitch = value => {
    this.setState({ notification: value });
  };

  render() {
    const { navigation } = this.props;
    const { userData, loading, notification } = this.state;
    return (
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        forceInset={{ top: "always" }}
      >
        <Header
          title="Profile"
          renderRight={() => {
            return <View />;
          }}
          renderRightSecond={() => {
            return <View />;
          }}
          onPressRight={() => {}}
          onPressRightSecond={() => {}}
        />
        <ScrollView>
          <View style={styles.contain}>
            <ProfileDetail
              image={userData.photoURL ? userData.photoURL : 122}
              textFirst={userData.displayName}
              point={"9.5"}
              textSecond={userData.address ? userData.address : ""}
              textThird={userData.email}
              onPress={() => navigation.navigate("ProfileExanple")}
            />
            {/* <ProfilePerformance
              data={[{value: "97.01%", title: "Feedback"}],[{value: "999", title: "Items"}],[{value: "120k", title: "Followers"}]}
              style={{ marginTop: 20, marginBottom: 20 }}
            /> */}
            <View style={{ width: "100%" }}>
              <TouchableOpacity
                style={styles.profileItem}
                onPress={() => {
                  navigation.navigate("ProfileEdit");
                }}
              >
                <Text body1>Edit Profile</Text>
                <Icon
                  name="angle-right"
                  size={18}
                  color={BaseColor.primaryColor}
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={styles.profileItem}
                onPress={() => {
                  navigation.navigate("ChangePassword");
                }}
              >
                <Text body1>Change Password</Text>
                <Icon
                  name="angle-right"
                  size={18}
                  color={BaseColor.primaryColor}
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity> */}
              {/*<TouchableOpacity*/}
              {/*  style={styles.profileItem}*/}
              {/*  onPress={() => {*/}
              {/*    navigation.navigate("ChangeLanguage");*/}
              {/*  }}*/}
              {/*>*/}
              {/*  <Text body1>Language</Text>*/}
              {/*  <View*/}
              {/*    style={{*/}
              {/*      flexDirection: "row",*/}
              {/*      alignItems: "center"*/}
              {/*    }}*/}
              {/*  >*/}
              {/*    <Text body1 grayColor>*/}
              {/*      English*/}
              {/*    </Text>*/}
              {/*    <Icon*/}
              {/*      name="angle-right"*/}
              {/*      size={18}*/}
              {/*      color={BaseColor.primaryColor}*/}
              {/*      style={{ marginLeft: 5 }}*/}
              {/*    />*/}
              {/*  </View>*/}
              {/*</TouchableOpacity>*/}
              {/* <TouchableOpacity style={styles.profileItem}
                onPress={() => {
                  navigation.navigate("Whislist");
                }}
              >
                <Text body1>Whislist</Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text body1 grayColor>
                    109
                  </Text>
                  <Icon
                    name="angle-right"
                    size={18}
                    color={BaseColor.primaryColor}
                    style={{ marginLeft: 5 }}
                  />
                </View>
              </TouchableOpacity>
              <View style={styles.profileItem}>
                <Text body1>Notification</Text>
                <Switch
                  name="angle-right"
                  size={18}
                  onValueChange={this.toggleSwitch}
                  value={notification}
                />
              </View> */}
              <TouchableOpacity
                style={styles.profileItem}
                onPress={() => {
                  navigation.navigate("ContactUs");
                }}
              >
                <Text body1>ContactUs</Text>
                <Icon
                  name="angle-right"
                  size={18}
                  color={BaseColor.primaryColor}
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.profileItem}
                onPress={() => {
                  navigation.navigate("AboutUs");
                }}
              >
                <Text body1>About Us</Text>
                <Icon
                  name="angle-right"
                  size={18}
                  color={BaseColor.primaryColor}
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={styles.profileItem}
              >
                <Text body1>Version</Text>
                <Text body1 grayColor>
                  {BaseSetting.appVersion}
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </ScrollView>
        <View style={{ padding: 20 }}>
          <Button
            full
            loading={loading}
            onPress={() => {
              this.btnLogOut();
            }}
          >
            Sign Out
          </Button>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = ({ netInfo, config }) => ({
  netInfo,
  facebookId: Config.Local.enable
    ? Config.Local.general.Facebook.logInID
    : config.general.Facebook.logInID
});

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(AuthActions, dispatch)
  };
};

export default connect(mapStateToProps, { mapDispatchToProps, clearUserData })(
  Profile
);
