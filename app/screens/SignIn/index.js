import React, { Component } from "react";
import { connect } from "react-redux";
import { AuthActions } from "@actions";
import { View, ScrollView, TouchableOpacity, TextInput } from "react-native";
import { BaseStyle, BaseColor } from "@config";
import { Events, Config, Languages } from "@common";
import { fetchUserData, fetchPostsBookmark } from "@redux/actions";
import { FacebookAPI, User } from "@services";
import { Header, SafeAreaView, Icon, Text, Button } from "@components";
import styles from "./styles";
import { store } from "app/store";
import { StackActions, NavigationActions } from "react-navigation";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      //errorMsg:"",
      success: {
        email: true,
        password: true
      }
    };
  }

  updateLoading = status => this.setState({ loading: status });

  loggedInSuccessfully = () => {
    const { navigation } = this.props;
    Events.toast(Languages.loginSuccess, "success");
    User.clearPosts();
    this.props.fetchPostsBookmark();

    this.props.fetchUserData().then(result => {
      if (store.getState().user.data != null) {
        if (
          typeof store.getState().user.data.jwtToken != "undefined" &&
          store.getState().user.data.jwtToken != null
        ) {
          this.updateLoading(false);
          console.log("warn", navigation);
          
          navigation.push("Profile", { login: true });
        } else {
          this.updateLoading(true);
          this.loggedInSuccessfully();
        }
      } else {
        this.updateLoading(true);
        this.loggedInSuccessfully();
      }
    });
  };

  loginFailure = error => {
    this.updateLoading(false);
    console.log("error.message", error.message);
    alert(error.message);
    // this.setState({
    //   errorMsg:error.message.replace( /(<([^>]+)>)/ig, '')
    // })
    Events.toast(error.message);
  };

  btnLogIn = async () => {
    const { netInfo } = this.props;
    if (!netInfo.isConnected) {
      return Events.toast(Languages.noConnection);
    }
    this.updateLoading(true);
    console.log("this.state", this.state);
    User.login(
      this.state.email.trim(),
      this.state.password,
      this.loggedInSuccessfully,
      this.loginFailure
    );
  };

  logInWithFacebook = async () => {
    const { facebookId } = this.props;
    this.setState({ fbLoading: true });
    const result = await FacebookAPI.login(facebookId);

    if (result.type === "success") {
      const token = result.token;
      User.loginFacebook(token.toString())
        .then(data => {
          if (data) {
            this.loggedInSuccessfully();
          }
        })
        .catch(err => {
          this.setState({ fbLoading: false });
          this.loginFailure(err);
        });
    } else {
      this.setState({ fbLoading: false });
    }
  };

  onLogin() {
    const { email, password, success } = this.state;
    const { navigation } = this.props;
    if (email == "" || password == "") {
      this.setState({
        success: {
          ...success,
          email: false,
          password: false
        }
      });
    } else {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props.actions.authentication(true, response => {
            if (response.success && email == "test" && password == "123456") {
              navigation.navigate("Profile");
            } else {
              this.setState({
                loading: false
              });
            }
          });
        }
      );
    }
  }

  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        forceInset={{ top: "always" }}
      >
        <Header
          title="Sign In"
          renderLeft={() => {
            return (
              <Icon
                name="arrow-left"
                size={20}
                color={BaseColor.primaryColor}
              />
            );
          }}
          onPressLeft={() => {
            navigation.goBack();
          }}
        />
        <ScrollView>
          <View style={styles.contain}>
            <TextInput
              style={[BaseStyle.textInput, { marginTop: 65 }]}
              onChangeText={text => this.setState({ email: text })}
              onFocus={() => {
                this.setState({
                  success: {
                    ...this.state.success,
                    email: true
                  }
                });
              }}
              autoCorrect={false}
              placeholder="Username/Email"
              placeholderTextColor={
                this.state.success.email
                  ? BaseColor.grayColor
                  : BaseColor.primaryColor
              }
              value={this.state.email}
              selectionColor={BaseColor.primaryColor}
            />
            <TextInput
              style={[BaseStyle.textInput, { marginTop: 10 }]}
              onChangeText={text => this.setState({ password: text })}
              onFocus={() => {
                this.setState({
                  success: {
                    ...this.state.success,
                    password: true
                  }
                });
              }}
              autoCorrect={false}
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor={
                this.state.success.password
                  ? BaseColor.grayColor
                  : BaseColor.primaryColor
              }
              value={this.state.password}
              selectionColor={BaseColor.primaryColor}
            />
            <View style={{ width: "100%" }}>
              <Button
                full
                loading={this.state.loading}
                style={{ marginTop: 20 }}
                onPress={() => {
                  this.btnLogIn();
                }}
              >
                Sign In
              </Button>
            </View>
            {/* {
            this.state.errorMsg !=""?
            <Text style={{color:'red',textAlign:'center',marginTop:10}}>{this.state.errorMsg}</Text>:null
            } */}
            <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
              <Text body1 grayColor style={{ marginTop: 25 }}>
                Register
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate("ResetPassword")}
            >
              <Text body1 grayColor style={{ marginTop: 25 }}>
                Forgot your password?
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = ({ netInfo, config }) => ({
  netInfo,
  facebookId: Config.Local.enable
    ? Config.Local.general.Facebook.logInID
    : config.general.Facebook.logInID
});

export default connect(mapStateToProps, { fetchUserData, fetchPostsBookmark })(
  SignIn
);
