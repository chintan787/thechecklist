import React, { Component } from "react";
import { View, ScrollView, Image, TextInput } from "react-native";
import { BaseStyle, BaseColor } from "@config";
import { Header, SafeAreaView, Icon, Text, Button } from "@components";
import styles from "./styles";
import { User } from '@services';

export default class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            loading: false,
            success: {
                email: true
            }
        };
    }

    loggedInSuccessfully = (success) => {
        this.setState({ loading: false, email: ""})
        alert(success);

    }

    loginFailure = (error) => {
        this.setState({ loading: false, email: ""})
        console.log('Reset password error.message', error);
        alert(error);
    }

    btnResetPassword = async () => {
        this.setState({loading: true})
        let { email, success } = this.state;
        if (email == "") {
            this.setState({
                success: {
                    ...success,
                    email: email != "" ? true : false,
                }
            });
            alert('Please enter your email address');
            this.setState({ loading: false })
        } else {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
            if(reg.test(this.state.email) === false)
            {
                alert('Please enter valid email address');
                this.setState({ loading: false, email: '' });
            }else{
                User.forgotPassword(
                    this.state.email.trim(),
                    this.loggedInSuccessfully,
                    this.loginFailure
                );
            }
        }
    }

    onReset() {
        const { navigation } = this.props;
        if (this.state.email == "") {
            this.setState({
                success: {
                    ...this.state.success,
                    email: false
                }
            });
        } else {
            this.setState(
                {
                    loading: true
                },
                () => {
                    setTimeout(() => {
                        this.setState({
                            loading: false
                        });
                        navigation.navigate("SignIn");
                    }, 500);
                }
            );
        }
    }

    render() {
        const { navigation } = this.props;
        let { loading, email, success } = this.state;
        return (
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                forceInset={{ top: "always" }}
            >
                <Header
                    title="Reset Password"
                    renderLeft={() => {
                        return (
                            <Icon
                                name="arrow-left"
                                size={20}
                                color={BaseColor.primaryColor}
                            />
                        );
                    }}
                    onPressLeft={() => {
                        navigation.goBack();
                    }}
                />
                <ScrollView>
                    <View
                        style={{
                            alignItems: "center",
                            padding: 20,
                            width: "100%"
                        }}
                    >
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 65 }]}
                            onChangeText={text => this.setState({ email: text })}
                            autoCorrect={false}
                            placeholder="Email Address"
                            keyboardType="email-address"
                            placeholderTextColor={
                                success.email
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={email}
                            selectionColor={BaseColor.primaryColor}
                        />
                        <View style={{ width: "100%" }}>
                            <Button
                                full
                                style={{ marginTop: 20 }}
                                onPress={() => {
                                    this.btnResetPassword();
                                }}
                                loading={loading}
                            >
                                Reset Password
                            </Button>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}
