import React, { Component } from "react";
import {
  FlatList,
  RefreshControl,
  ActivityIndicator,
  View,
  Animated,
  Platform,
  Text,
  TouchableOpacity,
  PermissionsAndroid
} from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker, Polyline } from "react-native-maps";
import { Config, Images, Tools, Constants } from "@common";
import { fetchPosts, searchPosts } from "@redux/actions";
import { connect } from "react-redux";
import { store } from "app/store";
import Geolocation from '@react-native-community/geolocation';
import MapViewDirections from 'react-native-maps-directions';
import { BaseStyle, BaseColor } from "@config";
import {
  Header,
  SafeAreaView,
  Icon,
  PlaceItem,
  FilterSort,
  CardList
} from "@components";
import styles from "./styles";
import * as Utils from "@utils";
import { NavigationEvents } from "react-navigation";

// Load sample data
import { PlaceListData } from "@data";

class Place extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.morepage = 1;

    //this.onEndReachedCalledDuringMomentum = true;
    const scrollAnim = new Animated.Value(0);
    const offsetAnim = new Animated.Value(0);

    this.state = {
      data: [],
      moreLoading: false,
      loading: true,
      toogleMore: false,
      refreshing: false,
      emptyData: false,
      modeView: "block",
      termId: null,
      mapView: false,
      page: 1,
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.009,
        longitudeDelta: 0.004
      },
      currentLocation: {
        latitude: null,
        longitude: null
      },
      distance:"",
      coordinate:[],
      list: PlaceListData,
      scrollAnim,
      offsetAnim,
      clampedScroll: Animated.diffClamp(
        Animated.add(
          scrollAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolateLeft: "clamp"
          }),
          offsetAnim
        ),
        0,
        40
      )
    };
    this.onChangeView = this.onChangeView.bind(this);
    this.onFilter = this.onFilter.bind(this);
    this.onChangeSort = this.onChangeSort.bind(this);
    this.onChangeMapView = this.onChangeMapView.bind(this);
    this.loadMoreData = this.loadMoreData.bind(this);
  }

  componentDidMount(props) {
    if (this.props.navigation.getParam("searchText") == undefined) {
      const termId = this.props.navigation.getParam("itemId", null);
      this.setState({ termId: termId });
      this.morepage = 1;
      this.renderPosts(termId);
    }
    else{
      if (this.props.navigation.getParam("searchText") !== "") {
        const searchText = this.props.navigation.getParam("searchText", null);
        this.morepage = 1;
        this.ScrollTop();
        this.rendersearchPosts(searchText);
      } 
    }
    this.requestLocationPermission();
    
  }

  requestLocationPermission = async () => {
    const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
    if(granted){
      this.callLocation();
    }else{
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,{
            'title': 'Location Access Required',
            'message': 'This App needs to Access your location'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //To Check, If Permission is granted
          this.callLocation();
        }
      } catch (err) {
        alert("err",err);
        console.warn(err)
      }
    }
  }

  callLocation = () => {
    Geolocation.getCurrentPosition(
      //Will give you the current location
       (position) => {
          const currentLongitude = JSON.stringify(position.coords.longitude);
          //getting the Longitude from the location json
          const currentLatitude = JSON.stringify(position.coords.latitude);
          let userlocation = {
            latitude: currentLatitude,
            longitude: currentLongitude
          }
          //getting the Latitude from the location json
          this.setState({ currentLocation: userlocation });
       },
       (error) => alert(error.message),
       { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  } 

 async componentDidUpdate(prevProps) {
    if (this.props.navigation.getParam("itemId") !== prevProps.navigation.getParam("itemId")) {
      this.setState({ termId: this.props.navigation.getParam("itemId") });
      const termId = this.props.navigation.getParam("itemId", null);
      this.morepage = 1;
      this.ScrollTop();
      this.setState({
        loading: true,
        emptyData:false,
      });
      this.renderPosts(termId);
    }
    if (this.props.navigation.getParam("searchText") !==prevProps.navigation.getParam("searchText")) {
      const searchText = this.props.navigation.getParam("searchText", null);
      this.morepage = 1;
      this.ScrollTop();
      this.setState({
        loading: true,
        emptyData:false,
      });
      this.rendersearchPosts(searchText);
    }
    const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
    if(!granted){
      this.requestLocationPermission();
    }
  }

  renderPosts(termId, scrollRender = false) {
    let { data } = this.state;
    let { navigation } = this.props;
    let page;

    let groupFetch, categoryFetch;
    groupFetch = navigation.getParam("groupFetch");
    categoryFetch = navigation.getParam("categoryFetch");

    if (scrollRender) {
      this.morepage++;
      page = this.morepage;
    } else {
      page = this.page;
    }

    if (termId != undefined) {
      if (groupFetch) {
        this.props.fetchPosts(page, Constants.Components.listing, null, termId).then(result => {
            if (result._paging.totalPages > 1) {
              this.setState({
                toogleMore: true
              });
            }
            if (result._paging.totalPages == page) {
              this.setState({
                toogleMore: false
              });
            }
            if (!scrollRender) {
              this.setState({
                data: result,
                loading: false
              });
            } else {
              if (page <= result._paging.totalPages) {
                const updatedData = data.concat(result);
                this.setState({
                  data: updatedData,
                  moreLoading: false
                });
              }
            }
          });
      } 
      else if (this.props.navigation.getParam("locationFetch") && this.props.navigation.getParam("categoryFetch")) {
        const locationId = this.props.navigation.getParam("locationId");
        this.props.fetchPosts(page,Constants.Components.listing,termId,null,null,null,locationId).then(result => {
          if(typeof result != undefined && result != ""  ){
          if (result._paging.totalPages > 1) {
            this.setState({
              toogleMore: true
            });
          }
          if (result._paging.totalPages == page) {
            this.setState({
              toogleMore: false
            });
          }
            if (!scrollRender) {
              this.setState({
                data: result,
                loading: false
              });
            } else {
              if (page <= result._paging.totalPages) {
                const updatedData = data.concat(result);
                this.setState({
                  data: updatedData,
                  moreLoading: false
                });
              }
            }
          }else{
            this.setState({
              data:result,
              emptyData: true,
              loading: false,
              toogleMore: false
              
            });
          }
          });
      }
      else if (categoryFetch) {
        this.props.fetchPosts(page, Constants.Components.listing, termId).then(result => {
          
          if(typeof result != undefined && result != ""  ){
            if (result._paging.totalPages > 1) {
              this.setState({
                toogleMore: true
              });
            }
            if (result._paging.totalPages == page) {
              this.setState({
                toogleMore: false
              });
            }

            if (!scrollRender) {
              this.setState({
                data: result,
                loading: false
              });
            } else {
              if (page <= result._paging.totalPages) {
                const updatedData = data.concat(result);
                this.setState({
                  data: updatedData,
                  moreLoading: false,
                  
                });
              }
            }
          }
          else{
            
            this.setState({
              data:result,
              emptyData: true,
              loading: false,
              toogleMore: false
              
            });
          }
            
          });
      } 
      else if (this.props.navigation.getParam("locationFetch")) {
        this.props.fetchPosts(page,Constants.Components.listing,null,null,null,null,termId).then(result => {
          if(typeof result != undefined && result != ""  ){
            if (result._paging.totalPages > 1) {
              this.setState({
                toogleMore: true
              });
            }
            if (result._paging.totalPages == page) {
              this.setState({
                toogleMore: false
              });
            }
            if (!scrollRender) {
              this.setState({
                data: result,
                loading: false
              });
            } else {
              if (page <= result._paging.totalPages) {
                const updatedData = data.concat(result);
                this.setState({
                  data: updatedData,
                  moreLoading: false
                });
              }
            }
          }else{
            this.setState({
              data:result,
              emptyData: true,
              loading: false,
              toogleMore: false
              
            });
          }
          });
      }
    }
    else {
      this.props.fetchPosts(page).then(result => {
        if (result != "" && typeof result != undefined) {
          if (result._paging.totalPages > 1) {
            this.setState({
              toogleMore: true
            });
          }
          if (result._paging.totalPages == page) {
            this.setState({
              toogleMore: false
            });
          }

          if (!scrollRender) {
            this.setState({
              data: result,
              loading: false
            });
          } else {
            if (page <= result._paging.totalPages) {
              const updatedData = data.concat(result);
              this.setState({
                data: updatedData,
                moreLoading: false
              });
            }
          }
        }else{
          this.setState({
            data:result,
            emptyData: true,
            loading: false,
            toogleMore: false
            
          });
        }
      });
    }
  }

  rendersearchPosts(searchText,scrollRender = false) {
    this.setState({
      emptyData: false,
      toogleMore: false
    });

   
    const { navigation } = this.props;
    const isMap = navigation.getParam("isMap");
    let { data } = this.state;
    if (isMap) {
      this.setState({
        mapView: true
      });
    }
    // let page;
    // if (scrollRender) {
    //   this.morepage++;
    //   page = this.morepage;
    // } else {
    //   page = this.page;
    // }

    this.props.searchPosts(isMap ? isMap : false, searchText).then(result => {
      
      if(typeof result != undefined && result != ""  ){
        if (!scrollRender) {
          this.setState(
            {
              data: result,
              loading: false
            },
            () => {
              this.onChangeMapView();
            }
          );
        }
      }else{
        this.setState({
          data:result,
          emptyData: true,
          loading: false,
          toogleMore: false  
        });
        if (isMap) {
          this.setState({
            mapView: false
          });
        }
      }
    });
  }
  loadMoreData() {
    this.setState({
      moreLoading: true
    });
    this.renderPosts(this.state.termId, true);
    // if (this.props.navigation.getParam("searchText") == undefined) {
    //     this.renderPosts(this.state.termId, true);   
    // }
    // else{
    //   if (this.props.navigation.getParam("searchText") !== "") {
    //     this.rendersearchPosts(this.props.navigation.getParam("searchText"),true)
    //   } 
    // }  
  }
  renderFooter() {
    return (
      //Footer View with Load More button
      <View style={styles.footer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={this.loadMoreData}
          //On Click of button calling loadMoreData function to load more data
          style={styles.loadMoreBtn}
        >
          <Text style={styles.btnText}>See More</Text>
          {this.state.moreLoading ? (
            <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
          ) : null}
        </TouchableOpacity>
      </View>
    );
  }

  onChangeSort(sort) {
    const { layouts } = this.props;
    let { data } = this.state;
    this.ScrollTop();
    if (sort.value == "old_post") {
      const getTimestamp = dateString => new Date(dateString).getTime();
      const isOlder = (object1, object2) =>
      getTimestamp(object1.date) < getTimestamp(object2.date) ? -1 : 1;
      const sortedArr = data.sort(isOlder);
      this.setState({
        data: sortedArr
      });
    } else {
      const getTimestamp = dateString => new Date(dateString).getTime();
      const isOlder = (object1, object2) =>
      getTimestamp(object2.date) < getTimestamp(object1.date) ? -1 : 1;
      const sortedArr = data.sort(isOlder);
      this.setState({
        data: sortedArr
      });
    }
  }

  ScrollTop() {
    this.flatListRef.scrollToOffset({ animated: false, offset: 0 });
  }

  onScreenRefresh() {
    //this.props.fetchPosts(2,Constants.Components.listing);
  }

  /**
   * @description Open modal when filterring mode is applied
   * @author Passion UI <passionui.com>
   * @date 2019-09-01
   */
  onFilter() {
    const { navigation, layouts } = this.props;
    const postList =
      typeof layouts.list !== "undefined" && layouts.list !== 0
        ? layouts.list
        : [];
    navigation.navigate("Filter", { postList: postList });
  }

  /**
   * @description Open modal when view mode is pressed
   * @author Passion UI <passionui.com>
   * @date 2019-09-01
   */
  onChangeView() {
    let { modeView } = this.state;
    Utils.enableExperimental();
    switch (modeView) {
      case "block":
        this.setState({
          modeView: "grid"
        });
        break;
      case "grid":
        this.setState({
          modeView: "list"
        });
        break;
      case "list":
        this.setState({
          modeView: "block"
        });
        break;
      default:
        this.setState({
          modeView: "block"
        });
        break;
    }
  }

  onChangeMapView() {
    const { mapView, data } = this.state;
    const { navigation, layouts } = this.props;

    const postList =
      typeof layouts.list !== "undefined" && layouts.list.length !== 0
        ? layouts.list
        : [];

    let { regionData } = {};
    if (data && data.length > 0) {
      regionData = {
        latitude: Number(data[0].geolocation_lat),
        longitude: Number(data[0].geolocation_long),
        latitudeDelta: 0.009,
        longitudeDelta: 0.004
      };

      this.setState({
        data: this.state.data.map((item, i) => {
          return {
            ...item,
            active:
              item.geolocation_lat == Number(data[0].geolocation_lat) &&
              item.geolocation_long == Number(data[0].geolocation_long)
          };
        }),
        region: regionData
      });
    }
    Utils.enableExperimental();
  }

  onSelectLocation(location) {
    for (let index = 0; index < this.state.data.length; index++) {
      const element = this.state.data[index];
      if (
        element.geolocation_lat == location.latitude &&
        element.geolocation_long == location.longitude
      ) {
        this.flatListRef.scrollToIndex({
          animated: true,
          index
        });
        this.setState({
          data: this.state.data.map((item, i) => {
            return {
              ...item,
              active:
                item.geolocation_lat == location.latitude &&
                item.geolocation_long == location.longitude
            };
          }),
          region: {
            latitudeDelta: 0.009,
            longitudeDelta: 0.004,
            latitude:
              this.state.data[index] &&
              Number(this.state.data[index].geolocation_lat),
            longitude:
              this.state.data[index] &&
              Number(this.state.data[index].geolocation_long)
          },
          distance: this.distance(location.latitude, location.longitude)
        });
      }
    }
  }

   distance = ( lat2, lon2, unit = "K") => {
    let { currentLocation } = this.state

    if ((currentLocation.latitude == lat2) && (currentLocation.longitude == lon2)) {
      return 0;
    }
    else {
      var radlat1 = Math.PI * currentLocation.latitude/180;
      var radlat2 = Math.PI * lat2/180;
      var theta = currentLocation.longitude-lon2;
      var radtheta = Math.PI * theta/180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180/Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit=="K") { dist = dist * 1.609344 }
      if (unit=="N") { dist = dist * 0.8684 }
      return `${Math.round(dist/1.609344)} Miles`;
    }
  }

  filterPostItem(post) {
    const { small } = Config.PostImage;
    const imageURL =
      typeof post !== "undefined"
        ? Tools.getImage(post, "large", true)
        : Images.imageBase;

    const title =
      typeof post !== "undefined"
        ? Tools.formatText(post.title.rendered, 300)
        : " ";
    const description =
      typeof post !== "undefined"
        ? Tools.getDescription(post.content, 200)
        : " ";
    const tagline = post ? post.listing_data._company_tagline : "";
    const rating = typeof post !== "undefined" ? post.totalRate : 0;
    const totalReview = typeof post !== "undefined" ? post.totalReview : 0;
    const price = typeof post !== "undefined" ? post.cost : "";
    const latitude =
      typeof post !== "undefined" ? post.address_lat * 1 : 37.763844;
    const longitude =
      typeof post !== "undefined" ? post.address_long * 1 : -122.414925;
    //const location = typeof post !== 'undefined' && post.listing_data.geolocation_formatted_address ? post.listing_data.geolocation_formatted_address : '';
    const location =
      typeof post !== "undefined" ? post.listing_data.geolocation_city : " ";
    const status =
      typeof post !== "undefined"
        ? post.listing_data._featured == "1"
          ? "Featured"
          : null
        : " ";
    const date = post.modified;

    return {
      imageURL,
      title,
      description,
      tagline,
      location,
      rating,
      totalReview,
      price,
      date,
      region: {
        latitude: latitude,
        longitude: longitude,
        latitudeDelta: 0.009,
        longitudeDelta: 0.004
      },
      status,
      rateStatus: "Very Good"
    };
  }

  /**
   * @description Render container view
   * @author Passion UI <passionui.com>
   * @date 2019-09-01
   * @returns
   */
  renderList() {
    const { modeView, refreshing, clampedScroll, data } = this.state;
    const { navigation, layouts, posts } = this.props;
    const navbarTranslate = clampedScroll.interpolate({
      inputRange: [0, 40],
      outputRange: [0, -40],
      extrapolate: "clamp"
    });
    const android = Platform.OS == "android";
    // const postList =
    //     typeof layouts.list !== 'undefined' && layouts.list !== 0
    //         ? layouts.list
    //         : []

    switch (modeView) {
      case "block":
        return (
          <View style={{ flex: 1, zIndex: 0 }}>
            {this.state.loading == true ? (
              <View style={[styles.indicatorContainer, { top: 0 }]}>
                <ActivityIndicator
                  animating={true}
                  color={"#33334b"}
                  style={styles.indicator}
                  size="large"
                />
              </View>
            ) : null}

            <FlatList
              ref={ref => {
                this.flatListRef = ref;
              }}
              contentInset={{ top: 50 }}
              contentContainerStyle={{
                zindex: 9,
                marginTop: android ? 50 : 0,
                paddingBottom: android ? 40 : 0
              }}
              refreshControl={
                <RefreshControl
                  colors={[BaseColor.primaryColor]}
                  tintColor={BaseColor.primaryColor}
                  refreshing={refreshing}
                  onRefresh={() => {
                    this.onScreenRefresh();
                  }}
                />
              }
              data={data}
              key={"block"}
              keyExtractor={(item, index) => item.id}
              //onEndReached = {() => {this.renderPosts(this.state.termId,true)}}
              ListEmptyComponent={
                this.state.emptyData ? (
                  <Text style={{ textAlign: "center" }}>No Places Found</Text>
                ) : null
              }
              ListFooterComponent={
                this.state.toogleMore ? this.renderFooter.bind(this) : null
              }
              renderItem={({ item, index }) => {
                const {
                  imageURL,
                  title,
                  description,
                  tagline,
                  location,
                  rating,
                  totalReview,
                  status,
                  rateStatus,
                  date
                } = this.filterPostItem(item);

                return (
                  <PlaceItem
                    block
                    postId={item.id}
                    image={{ uri: imageURL }}
                    title={title}
                    subtitle={tagline}
                    location={location}
                    rate={rating}
                    status={status}
                    rateStatus={rateStatus}
                    numReviews={totalReview}
                    date={date}
                    style={{
                      borderBottomWidth: 0.5,
                      borderColor: BaseColor.textSecondaryColor,
                      marginBottom: 10
                    }}
                    onPress={() =>
                      navigation.navigate("PlaceDetail", { postItem: item })
                    }
                    onPressTag={() => navigation.navigate("Review")}
                  />
                );
              }}
            />
            <Animated.View
              style={[
                styles.navbar,
                { transform: [{ translateY: navbarTranslate }] }
              ]}
            >
              <FilterSort
                modeView={modeView}
                onChangeSort={this.onChangeSort}
                onChangeView={this.onChangeView}
                onFilter={this.onFilter}
              />
            </Animated.View>
          </View>
        );
      case "grid":
        return (
          <View style={{ flex: 1 }}>
            {this.state.loading == true ? (
              <View style={[styles.indicatorContainer, { top: 0 }]}>
                <ActivityIndicator
                  animating={true}
                  color={"#33334b"}
                  style={styles.indicator}
                  size="large"
                />
              </View>
            ) : null}
            <FlatList
              ref={ref => {
                this.flatListRef = ref;
              }}
              contentInset={{ top: 50 }}
              contentContainerStyle={{
                marginTop: android ? 50 : 0,
                paddingBottom: android ? 40 : 0
              }}
              columnWrapperStyle={{
                marginHorizontal: 20
              }}
              refreshControl={
                <RefreshControl
                  colors={[BaseColor.primaryColor]}
                  tintColor={BaseColor.primaryColor}
                  refreshing={refreshing}
                  onRefresh={() => {
                    this.onScreenRefresh();
                  }}
                />
              }
              scrollEventThrottle={1}
              // onScroll={Animated.event(
              //   [
              //     {
              //       nativeEvent: {
              //         contentOffset: {
              //           y: this.state.scrollAnim
              //         }
              //       }
              //     }
              //   ],
              //   { useNativeDriver: true }
              // )}
              showsVerticalScrollIndicator={false}
              numColumns={2}
              data={data}
              key={"gird"}
              keyExtractor={(item, index) => item.id}
              ListEmptyComponent={
                this.state.emptyData ? (
                  <Text style={{ textAlign: "center" }}>No Places Found</Text>
                ) : null
              }
              ListFooterComponent={
                this.state.toogleMore ? this.renderFooter.bind(this) : null
              }
              renderItem={({ item, index }) => {
                const {
                  imageURL,
                  title,
                  description,
                  tagline,
                  location,
                  rating,
                  totalReview,
                  status,
                  rateStatus
                } = this.filterPostItem(item);
                return (
                  <PlaceItem
                    grid
                    postId={item.id}
                    image={{ uri: imageURL }}
                    title={title}
                    subtitle={tagline}
                    location={location}
                    rate={rating}
                    status={status}
                    rateStatus={rateStatus}
                    numReviews={totalReview}
                    style={
                      index % 2 == 0
                        ? {
                            marginBottom: 20
                          }
                        : {
                            marginLeft: 15,
                            marginBottom: 20
                          }
                    }
                    onPress={() =>
                      navigation.navigate("PlaceDetail", { postItem: item })
                    }
                    onPressTag={() => navigation.navigate("Review")}
                  />
                );
              }}
            />
            <Animated.View
              style={[
                styles.navbar,
                {
                  transform: [{ translateY: navbarTranslate }]
                }
              ]}
            >
              <FilterSort
                modeView={modeView}
                onChangeSort={this.onChangeSort}
                onChangeView={this.onChangeView}
                onFilter={this.onFilter}
              />
            </Animated.View>
          </View>
        );

      case "list":
        return (
          <View style={{ flex: 1 }}>
            {this.state.loading == true ? (
              <View style={[styles.indicatorContainer, { top: 0 }]}>
                <ActivityIndicator
                  animating={true}
                  color={"#33334b"}
                  style={styles.indicator}
                  size="large"
                />
              </View>
            ) : null}
            <FlatList
              ref={ref => {
                this.flatListRef = ref;
              }}
              style={{ paddingHorizontal: 20 }}
              contentInset={{ top: 50 }}
              contentContainerStyle={{
                marginTop: android ? 50 : 0,
                paddingBottom: android ? 50 : 0
              }}
              refreshControl={
                <RefreshControl
                  colors={[BaseColor.primaryColor]}
                  tintColor={BaseColor.primaryColor}
                  refreshing={refreshing}
                  onRefresh={() => {
                    this.onScreenRefresh();
                  }}
                />
              }
              scrollEventThrottle={1}
              // onScroll={Animated.event(
              //   [
              //     {
              //       nativeEvent: {
              //         contentOffset: {
              //           y: this.state.scrollAnim
              //         }
              //       }
              //     }
              //   ],
              //   { useNativeDriver: true }
              // )}
              data={data}
              key={"list"}
              keyExtractor={(item, index) => item.id}
              ListEmptyComponent={
                this.state.emptyData ? (
                  <Text style={{ textAlign: "center" }}>No Places Found</Text>
                ) : null
              }
              ListFooterComponent={
                this.state.toogleMore ? this.renderFooter.bind(this) : null
              }
              renderItem={({ item, index }) => {
                const {
                  imageURL,
                  title,
                  description,
                  tagline,
                  location,
                  rating,
                  totalReview,
                  status,
                  rateStatus
                } = this.filterPostItem(item);
                return (
                  <PlaceItem
                    list
                    postId={item.id}
                    image={{ uri: imageURL }}
                    title={title}
                    subtitle={tagline}
                    location={location}
                    rate={rating}
                    status={status}
                    rateStatus={rateStatus}
                    numReviews={totalReview}
                    style={{
                      marginBottom: 20
                    }}
                    onPress={() =>
                      navigation.navigate("PlaceDetail", { postItem: item })
                    }
                    onPressTag={() => navigation.navigate("Review")}
                  />
                );
              }}
            />
            <Animated.View
              style={[
                styles.navbar,
                {
                  transform: [{ translateY: navbarTranslate }]
                }
              ]}
            >
              <FilterSort
                modeView={modeView}
                onChangeSort={this.onChangeSort}
                onChangeView={this.onChangeView}
                onFilter={this.onFilter}
              />
            </Animated.View>
          </View>
        );
      default:
        return (
          <View style={{ flex: 1 }}>
            <FlatList
            ref={ref => {
              this.flatListRef = ref;
            }}
              contentInset={{ top: 50 }}
              contentContainerStyle={{
                marginTop: android ? 50 : 0,
                paddingBottom: android ? 40 : 0
              }}
              refreshControl={
                <RefreshControl
                  colors={[BaseColor.primaryColor]}
                  tintColor={BaseColor.primaryColor}
                  refreshing={refreshing}
                  onRefresh={() => {
                    this.onScreenRefresh();
                  }}
                />
              }
              scrollEventThrottle={1}
              // onScroll={Animated.event(
              //   [
              //     {
              //       nativeEvent: {
              //         contentOffset: {
              //           y: this.state.scrollAnim
              //         }
              //       }
              //     }
              //   ],
              //   { useNativeDriver: true }
              // )}
              data={data}
              key={"block"}
              keyExtractor={(item, index) => item.id}
              ListEmptyComponent={
                this.state.emptyData ? (
                  <Text style={{ textAlign: "center" }}>No Places Found</Text>
                ) : null
              }
              ListFooterComponent={
                this.state.toogleMore ? this.renderFooter.bind(this) : null
              }
              renderItem={({ item, index }) => {
                const {
                  imageURL,
                  title,
                  description,
                  tagline,
                  location,
                  rating,
                  totalReview,
                  status,
                  rateStatus
                } = this.filterPostItem(item);
                return (
                  <PlaceItem
                    block
                    postId={item.id}
                    image={{ uri: imageURL }}
                    title={title}
                    subtitle={tagline}
                    location={location}
                    rate={rating}
                    status={status}
                    rateStatus={rateStatus}
                    numReviews={totalReview}
                    style={{
                      borderBottomWidth: 0.5,
                      borderColor: BaseColor.textSecondaryColor,
                      marginBottom: 10
                    }}
                    onPress={() =>
                      navigation.navigate("PlaceDetail", { postItem: item })
                    }
                    onPressTag={() => navigation.navigate("Review")}
                  />
                );
              }}
            />
            <Animated.View
              style={[
                styles.navbar,
                { transform: [{ translateY: navbarTranslate }] }
              ]}
            >
              <FilterSort
                modeView={modeView}
                onChangeSort={this.onChangeSort}
                onChangeView={this.onChangeView}
                onFilter={this.onFilter}
              />
            </Animated.View>
          </View>
        );
        break;
    }
  }

  renderMapView() {
    const { navigation, layouts } = this.props;
    const { list, region, data } = this.state;
    const postList =
      typeof layouts.list !== "undefined" && layouts.list !== 0
        ? layouts.list
        : [];

    return (
      <View style={{ flex: 1 }}>
          {this.state.loading == true ? (
              <View style={[styles.indicatorContainer, { top: 0 }]}>
                <ActivityIndicator
                  animating={true}
                  color={"#33334b"}
                  style={styles.indicator}
                  size="large"
                />
              </View>
            ) : null}
        <MapView
          ref={map => (this.map = map)}
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={region}
          minZoomLevel={1}
        >
          {data && data.length > 0
            ? data.map(item => {
                const {
                  imageURL,
                  title,
                  description,
                  location,
                  rating,
                  totalReview,
                  status,
                  tagline
                } = this.filterPostItem(item);

                let { regionData } = {};
                if (typeof item != "undefined" && item != null) {
                  regionData = {
                    latitude: Number(item.geolocation_lat),
                    longitude: Number(item.geolocation_long),
                    latitudeDelta: 0.009,
                    longitudeDelta: 0.004
                  };
                }
                return (
                  <Marker
                    title={title}
                    onPress={e => {
                      console.log("ONPRESS MARKER", e.nativeEvent);

                      return this.onSelectLocation(e.nativeEvent.coordinate);
                    }}
                    key={item.id}
                    coordinate={regionData}
                  >
                    <View style={{ alignItems: "center" }}>

                    
                      <View
                        style={[
                          styles.iconLocation,
                          {
                            backgroundColor: item.active
                              ? BaseColor.primaryColor
                              : BaseColor.whiteColor
                          }
                        ]}
                      >
                        
                        <Icon
                          name="star"
                          size={16}
                          color={
                            item.active
                              ? BaseColor.whiteColor
                              : BaseColor.primaryColor
                          }
                        />
                      </View>
                      {item.active ? (
                       
                        <View
                          style={{
                            backgroundColor: BaseColor.primaryColor,
                            padding: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            marginTop: 5
                          }}
                        >
                          <Text style={{ color: "#fff" }}>{title}</Text>
                          <Text style={{ color: "#8b8ca1",textAlign:"center" }}>{this.state.distance || ""}</Text>
                          
                        </View>
                        
                       
                      ) : null}

                    </View>
                  </Marker>
                );
              })
            : null}
        </MapView>
        <View style={{ position: "absolute", bottom: 0 }}>
          <FlatList
            ref={ref => {
              this.flatListRef = ref;
            }}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            data={data}
            onMomentumScrollEnd={event => {
              const index = Number(
                event.nativeEvent.contentOffset.x / 300
              ).toFixed();
              this.setState(
                {
                  data: data.map((item, i) => {
                    return {
                      ...item,
                      active: i == index
                    };
                  }),
                  region: {
                    latitudeDelta: 0.009,
                    longitudeDelta: 0.004,
                    latitude:
                      data[index] && Number(data[index].geolocation_lat),
                    longitude:
                      data[index] && Number(data[index].geolocation_long)
                  },
                  distance: this.distance(data[index] && Number(data[index].geolocation_lat),data[index] && Number(data[index].geolocation_long))
                });
            }}
            keyExtractor={(item, index) => item.id}
            renderItem={({ item, index }) => {
              const {
                imageURL,
                title,
                description,
                tagline,
                location,
                rating,
                totalReview,
                status,
                rateStatus
              } = this.filterPostItem(item);
              return (
                <CardList
                  image={{ uri: imageURL }}
                  title={title}
                  subtitle={tagline}
                  distance={this.state.distance}
                  rate={rating}
                  style={{
                    padding: 10,
                    width: 300,
                    marginBottom: 20,
                    marginHorizontal: 10,
                    backgroundColor: BaseColor.whiteColor,
                    borderRadius: 8,
                    shadowColor: item.active
                      ? BaseColor.lightPrimaryColor
                      : "black",
                    shadowOffset: {
                      width: 0,
                      height: 2
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 5
                  }}
                  onPress={() =>
                    navigation.navigate("PlaceDetail", { postItem: item })
                  }
                  onPressTag={() => navigation.navigate("Review")}
                />
              );
            }}
          />
        </View>
      </View>
    );
  }

  nextPosts = () => {
    this.page += 1;
    !this.props.layouts.finish && this.fetchPost();
  };

  render() {
    const { navigation } = this.props;
    const { mapView } = this.state;

    return (
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        forceInset={{ top: "always" }}
      >
        <NavigationEvents
          onDidBlur={payload => {
            navigation.setParams({ groupFetch: false });
            navigation.setParams({ categoryFetch: false });
            navigation.setParams({ locationFetch: false });
            navigation.setParams({ searchText: "" });
            navigation.setParams({ isMap: false });
            this.setState({
              mapView:false
            })
          }}
        />
        <Header
          title="Place"
          renderRight={() => {
            return (
              <Icon
                name={mapView ? "align-right" : "map"}
                size={20}
                color={BaseColor.primaryColor}
              />
            );
          }}
          renderRightSecond={() => {
            return (
              <Icon name="search" size={24} color={BaseColor.primaryColor} />
            );
          }}
          onPressRightSecond={() => {
            navigation.navigate("SearchHistory");
          }}
          onPressLeft={() => {
            navigation.goBack();
          }}
          onPressRight={() => {
            this.setState({
              mapView: !mapView
            });
          }}
        />

        {mapView ? this.renderMapView() : this.renderList()}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = ({ homeLayout, posts }, ownProp) => {
  const index = ownProp.index;
  return {
    layouts: homeLayout[0],
    isloading: posts.isFetching
  };
};
export default connect(mapStateToProps, { fetchPosts, searchPosts })(Place);
