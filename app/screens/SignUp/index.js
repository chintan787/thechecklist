import React, { Component } from "react";
import { View, ScrollView, TextInput } from "react-native";
import { BaseStyle, BaseColor } from "@config";
import { Tools, Events, warn, Languages } from '@common'
import { User, WooWorker, WPUserAPI } from '@services'
import { connect } from 'react-redux'
import { fetchPostsBookmark, fetchUserData } from '@redux/actions'
import { Header, SafeAreaView, Icon, Button } from "@components";
import styles from "./styles";
import { store } from "app/store";

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            loading: false,
            success: {
                name: true,
                email: true,
                password: true
            }
        };
    }

    loggedInSuccessfully = () => {
        const { navigation } = this.props;
        Events.toast(Languages.loginSuccess,  'success')
        this.setState({ loading: false })
        User.clearPosts()
        this.props.fetchPostsBookmark()

        this.props.fetchUserData().then(result => {
            if (store.getState().user.data != null) {
                if (typeof store.getState().user.data.jwtToken != 'undefined' && store.getState().user.data.jwtToken != null 
                    && store.getState().user.data.email == this.state.email) {
                    navigation.navigate("Profile", {login:true});
                }else{
                    this.setState({ loading: true })
                    this.loggedInSuccessfully();
                }
            }else{
                this.setState({ loading: true })
                this.loggedInSuccessfully();
            }
        });
        /*setTimeout(() => {
            this.props.fetchPostsBookmark()
            this.props.fetchUserData()
        }, 500)*/
    }

    loginFailure = (error) => {
        this.setState({ loading: false })
        Events.toast(error.message)
        console.log('sign in error.message', error);
        alert(error);
    }

    btnSignUp = async () => {
        this.setState({loading: true})
        let { name, email, password, success } = this.state;

        if ((name != "" && password != "") || email != "") {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
            if(reg.test(this.state.email) === false)
            {
                alert('Please enter valid email address');
                this.setState({ loading: false, email: '' });
            }else{
                User.create(
                    this.state.email.trim(),
                    this.state.password,
                    this.state.name.trim(),
                    this.loggedInSuccessfully,
                    this.loginFailure
                )
            }
        }
        else {
            let { alertMsg } = "";
            this.setState({
                success: {
                    ...success,
                    name: name != "" ? true : false,
                    email: email != "" ? true : false,
                    password: password != "" ? true : false,
                }
            });
            if(name == ""){
                alertMsg = "Please enter your name";
            }else if(email == ""){
                alertMsg = "Please enter your email address";
            }else{
                alertMsg = "Please enter password";
            }
            alert(alertMsg);
            this.setState({ loading: false })

        }

        /*User.create(
            this.state.email,
            this.state.password,
            this.state.name,
            (user) => {
                if (user) {
                    // Events.closeUserModal()
                    this.loggedInSuccessfully()
                }
            },
            (error) => {
                this.loginFailure(error)
            }
        )*/
    }

    onSignUp() {
        const { navigation } = this.props;
        let { name, email, success } = this.state;


        if (name == "" || email == "") {
            this.setState({
                success: {
                    ...success,
                    name: name != "" ? true : false,
                    email: email != "" ? true : false
                }
            });
        } else {
            this.setState(
                {
                    loading: true
                },
                () => {
                    setTimeout(() => {
                        this.setState({
                            loading: false
                        });
                        navigation.navigate("SignIn");
                    }, 500);
                }
            );
        }
    }

    render() {
        const { navigation } = this.props;
        let { loading, name, email, password, success } = this.state;
        return (
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                forceInset={{ top: "always" }}
            >
                <Header
                    title="Sign Up"
                    renderLeft={() => {
                        return (
                            <Icon
                                name="arrow-left"
                                size={20}
                                color={BaseColor.primaryColor}
                            />
                        );
                    }}
                    onPressLeft={() => {
                        navigation.goBack();
                    }}
                />
                <ScrollView>
                    <View style={styles.contain}>
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 65 }]}
                            onChangeText={text => this.setState({ name: text })}
                            autoCorrect={false}
                            placeholder="Name"
                            placeholderTextColor={
                                success.name
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={name}
                        />
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ email: text })}
                            autoCorrect={false}
                            placeholder="Email"
                            keyboardType="email-address"
                            placeholderTextColor={
                                success.email
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={email}
                        />
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text =>
                                this.setState({ password: text })
                            }
                            autoCorrect={false}
                            placeholder="Password"
                            secureTextEntry={true}
                            placeholderTextColor={
                                success.password
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={password}
                        />
                        {/* <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text =>
                                this.setState({ address: text })
                            }
                            autoCorrect={false}
                            placeholder="Address"
                            placeholderTextColor={
                                success.address
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={address}
                        /> */}
                        <View style={{ width: "100%" }}>
                            <Button
                                full
                                style={{ marginTop: 20 }}
                                loading={loading}
                                onPress={() => this.btnSignUp()}
                            >
                                Sign Up
                            </Button>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

export default connect(
    null,
    { fetchPostsBookmark, fetchUserData }
)(SignUp)
