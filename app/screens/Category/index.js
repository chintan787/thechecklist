import React, { Component } from "react";
import { FlatList, RefreshControl, View, TextInput,Text } from "react-native";
import { connect } from 'react-redux'
import flatten from 'lodash/flatten'
import { BaseStyle, BaseColor } from "@config";
import {
  Header,
  SafeAreaView,
  Icon,
  CategoryFull,
  CategoryIcon
} from "@components";
import * as Utils from "@utils";
import { CategoryData } from "@data";
import { fetchCategories, fetchPosts, setActiveCategory } from '@redux/actions'
import { Tools, Languages, Images, AppConfig } from '@common'
import styles from "./styles";

class Category extends Component {
  constructor(props) {
    super(props);

    // Define list Category screens
    this.state = {
      refreshing: false,
      search: "",
      modeView: "full",
      categories: this.props.categories
    };
  }

  componentDidMount() {
    const { fetchCategories } = this.props
    fetchCategories()
  }

  onChangeView() {
    let { modeView } = this.state;
    Utils.enableExperimental();
    switch (modeView) {
      case "full":
        this.setState({
          modeView: "icon"
        });
        break;
      case "icon":
        this.setState({
          modeView: "full"
        });
        break;
    }
  }
  OnSearch(text) {
    let { search } = this.state;
    let { categories } = this.props;
    this.setState({
      search: text,
      categories: categories,
    });
    const newData = categories.filter(function(item) {
      const itemData = item.name  ? item.name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.startsWith(textData);
    });
 
    this.setState({
      categories: newData,
    });
  }

  renderItem(item, index) {
    const { navigation } = this.props;
    const { modeView } = this.state;
    // optimize image
    let cateImage = "";
    if (item.image && item.image[0]) {
      let temp = item.image[0].file
      cateImage = temp ? `${AppConfig.Website.url}/wp-content/uploads/${temp}` : "";
    } else {
      cateImage = Images.imageBase;
    }

    item.icon = "tag";
    switch (modeView) {
      case "icon":
        return (
          <CategoryIcon
            icon={item.icon}
            title={item.name}
            subtitle={item.subtitle}
            onPress={() => navigation.navigate("Place",{itemId: item.id,categoryFetch:true})}
            style={{
              marginBottom: 10,
              borderBottomWidth: 0.5,
              paddingBottom: 10,
              borderColor: BaseColor.textSecondaryColor
            }}
          />
        );
      case "full":
        return (
          <CategoryFull
            image={{ uri: cateImage }}
            icon={item.icon}
            title={item.name}
            subtitle={item.subtitle}
            onPress={() => navigation.navigate("Place",{itemId: item.id,categoryFetch:true})}
            style={{
              marginBottom: 10
            }}
          />
        );
      default:
        break;
    }
  }
  render() {
    const { navigation, categories } = this.props;
    let { search, category, modeView } = this.state;
    return (
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        forceInset={{ top: "always" }}
      >
        <Header
          title="Category"
          renderRight={() => {
            return (
              <Icon
                name={modeView == "full" ? "th-large" : "th-list"}
                size={20}
                color={BaseColor.grayColor}
              />
            );
          }}
          onPressRight={() => this.onChangeView()}
        />
        <View style={{ padding: 20 }}>
          <TextInput
            style={BaseStyle.textInput}
            onChangeText={text => this.OnSearch(text)}
            autoCorrect={false}
            placeholder="Search"
            placeholderTextColor={BaseColor.grayColor}
            value={search}
            selectionColor={BaseColor.primaryColor}
            onSubmitEditing={() => {}}
          />
        </View>
        <FlatList
          contentContainerStyle={{
            marginHorizontal: 20
          }}
          refreshControl={
            <RefreshControl
              colors={[BaseColor.primaryColor]}
              tintColor={BaseColor.primaryColor}
              refreshing={this.state.refreshing}
              onRefresh={() => {}}
            />
          }
          enableEmptySections={true}
          ListEmptyComponent={<Text style={{textAlign:'center'}}>No Category found</Text>}
          data={this.state.categories}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) => this.renderItem(item, index)}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  const categories = flatten(state.categories.list)
  return { categories }
}

export default connect(
    mapStateToProps,
    {
      fetchCategories,
      fetchPosts,
      setActiveCategory,
    }
)(Category)
