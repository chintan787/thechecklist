/** @format */

export default class Group {
    constructor(group) {
      const { id, count, name, slug, taxonomy, term_image } = group
  
      try {
        this.id = id
        this.count = count
        this.name = name
        this.slug = slug
        this.taxonomy = taxonomy
        this.image = term_image ? term_image : null
      } catch (e) {
        console.error(['err construct group', e.message])
      }
      // warn(this)
    }
  }
  