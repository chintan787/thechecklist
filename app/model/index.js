/** @format */

import _News from './News'
export const News = _News

import _Photo from './Photo'
export const Photo = _Photo

import _Category from './Category'
export const Category = _Category

import _Group from './Group'
export const Group = _Group
