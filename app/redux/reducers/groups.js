/** @format */

import {
    GROUP_FETCH_SUCCESS,
    GROUP_ON_SELECT,
    GROUP_SELECT_LAYOUT,
    GROUP_FETCH_MORE_SUCCESS,
  } from '@redux/types'
  
  import { Config } from '@common'
  import { flatten } from 'lodash'
  
  const initialState = {
    isFetching: false,
    error: null,
    list: []
  }
  
  export default (state = initialState, action) => {
    const { payload, type } = action
  
    switch (type) {
      case GROUP_FETCH_SUCCESS:
        if (
          typeof payload == 'undefined' ||
          payload == null ||
          payload.length == 0
        ) {
          return state
        }
        return {
          ...state,
          error: null,
          isFetching: false,
          list: payload || [],
        }
      case GROUP_ON_SELECT:
        return {
          ...state,
          isFetching: false,
          selectedCategory: payload || null,
        }
      case GROUP_SELECT_LAYOUT:
        return {
          ...state,
          isFetching: false,
          selectedLayout: payload || false,
        }
      case GROUP_FETCH_MORE_SUCCESS: {
        return {
          ...state,
          error: null,
          isFetching: false,
          list: state.list.concat(flatten(payload)),
        }
      }
  
      default:
        return state
    }
  }
  