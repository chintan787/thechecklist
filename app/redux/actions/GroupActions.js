/** @format */

import wp from '@services/WPAPI'
import fetch from './fetch'
import { Config } from '@common'

import {
  GROUP_FETCH_SUCCESS,
  GROUP_FETCH_MORE_SUCCESS,
} from '@redux/types'

export const fetchGroups = (page = 1,number=100) => {
  let api = wp
    .getJobListingGroup()
    .hide_empty(true)
    .page(page)
    .embed()
    if (Config.Groups.showAll) {
      api
        .page(page)
        .per_page(number)
        .embed()
    } else if (!Config.Groups.showSub) {
      api.parent(0)
    }
  return (dispatch) => {
    if (page == 1) {
      fetch(dispatch, api, GROUP_FETCH_SUCCESS)
      } else {
        fetch(dispatch, api, GROUP_FETCH_MORE_SUCCESS)

      }
    
  }
}

