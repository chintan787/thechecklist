import {
  BaseColor,
  BlueColor,
  PinkColor,
  GreenColor,
  YellowColor
} from "@config";

export const HomeServicesData = [
  {
    id: "1",
    color: BaseColor.lightPrimaryColor,
    icon: "shopping-bag",
    name: "Shopping",
    route: "Place"
  },
  {
    id: "2",
    color: BaseColor.kashmir,
    icon: "coffee",
    name: "Coffee & Bar",
    route: "Place"
  },
  {
    id: "3",
    color: PinkColor.primaryColor,
    icon: "star",
    name: "Events",
    route: "Place"
  },
  {
    id: "4",
    color: BlueColor.primaryColor,
    icon: "handshake",
    name: "Real Estate",
    route: "Place"
  },
  {
    id: "5",
    color: BaseColor.accentColor,
    icon: "briefcase",
    name: "Jobseeker",
    route: "Place"
  },
  {
    id: "3",
    color: GreenColor.primaryColor,
    icon: "utensils",
    name: "Restaurant",
    route: "Place"
  },
  {
    id: "4",
    color: YellowColor.primaryColor,
    icon: "car",
    name: "Automotive",
    route: "Place"
  },
  {
    id: "5",
    color: BaseColor.kashmir,
    icon: "ellipsis-h",
    name: "More",
    route: "Category"
  }
];
