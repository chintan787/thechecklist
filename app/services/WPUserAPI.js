/**
 * Created by InspireUI on 01/03/2017.
 * An API for JSON API Auth Word Press plugin.
 * https://wordpress.org/plugins/json-api-auth/
 */

import { request, error, AppConfig } from '@common'

const url = AppConfig.Website.url
const isSecured = url.startsWith('https')
const secure = isSecured ? '' : '&insecure=cool'
const cookieLifeTime = 120960000000

const WPUserAPI = {
  login: async (username, password) => {
    const nonce = 'inspireui';
    const _url = `${url}/rapi/mstore_user/generate_auth_cookie/?second=${cookieLifeTime}&username=${username}&password=${password}${secure}&nonce=${nonce}`
    return await request(_url)
  },
  loginFacebook: async token => {
    const _url = `${url}/rapi/mstore_user/fb_connect/?second=${cookieLifeTime}&access_token=${token}${secure}`
    return await request(_url)
  },

  register: async (email, password, name) => {
    try {
      const nonce = await WPUserAPI.getNonce()
      let _url =
        `${url}/rapi/mstore_user/register/?` +
        `username=${email}` +
        `&email=${email}` +
        `&display_name=${name}` +
        `&user_nicename=${name}` +
        `&first_name=${name}` +
        `&user_pass=${password}` +
        `&nonce=${nonce}` +
        '&notify=both' +
        secure
      return await request(_url)
    } catch (err) {
      error(err)
      return { error: err }
    }
  },
  getNonce: async () => {
    const _url = `${url}/rapi/get_nonce/?controller=mstore_user&method=register`
    const json = await request(_url)
    return json && json.nonce
  },
}

export default WPUserAPI
