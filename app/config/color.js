/**
 * Color Palette Define
 */

let OrangeColor = {
    primaryColor: "#E5634D",
    darkPrimaryColor: "#C31C0D",
    lightPrimaryColor: "#FF8A65",
    accentColor: "#4A90A4"
};

let BlueColor = {
    primaryColor: "#5DADE2",
    darkPrimaryColor: "#1281ac",
    lightPrimaryColor: "#68c9ef",
    accentColor: "#FF8A65"
};

let PinkColor = {
    primaryColor: "#A569BD",
    darkPrimaryColor: "#C2185B",
    lightPrimaryColor: "#F8BBD0",
    accentColor: "#8BC34A"
};

let GreenColor = {
    primaryColor: "#58D68D",
    darkPrimaryColor: "#388E3C",
    lightPrimaryColor: "#C8E6C9",
    accentColor: "#607D8B"
};

let YellowColor = {
    primaryColor: "#FDC60A",
    darkPrimaryColor: "#FFA000",
    lightPrimaryColor: "#FFECB3",
    accentColor: "#795548"
};

/**
 * Main color use for whole application
 */
let BaseColor = {
    ...OrangeColor,
    ...{
        kashmir: "#5D6D7E",
        primaryColor: "#33334b", // primary color for your app, usually your brand color.
        darkPrimaryColor: "#33334b", // color is darker base on BaseColor.primaryColor
        lightPrimaryColor: "#383753", // // color is lighter base on BaseColor.primaryColor
        accentColor: "#4A90A4", // secondary color for your app which complements the primary color.
        textPrimaryColor: "#212121", // text color for content.
        textSecondaryColor: "#E0E0E1", // text color for content.
        grayColor: "#9B9B9B", // gray color, just common using for app
        darkBlueColor: "#24253D", // dark blue color, just common using for app
        dividerColor: "#BDBDBD", // color for separator
        whiteColor: "#FFFFFF", // white color
        fieldColor: "#F5F5F5", // common field color like input, text area
        yellowColor: "#FDC60A", // just another color use for the app
        navyBlue: "#3C5A99" // just another color use for the app
    }
};

export {
    BaseColor,
    OrangeColor,
    BlueColor,
    PinkColor,
    GreenColor,
    YellowColor
}
